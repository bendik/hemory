import {HTML, render, html} from 'ube'

const ANIMALS = "🐒 🦍 🐕 🐩 🐺 🦊 🦝 🐱 🐈 🐯 🐆 🐎 🦄 🦓 🦌 🐮 🐂 🐃 🐄 🐖 🐗 🐏 🐑 🐐 🐪 🐫 🦙 🦒 🐘 🦏 🦛 🐭 🐁 🐀 🐹 🐰 🐇 🐿 🦔 🦇 🐻 🐨 🐼 🦘 🦡 🦃 🐔 🐓 🐣 🐤 🐦 🐧 🕊 🦅 🦆 🦢 🦉 🦚 🦜 🐸 🐊 🐢 🦎 🐍 🐉 🦕 🦖 🐳 🐋 🐬 🐟 🐠 🐡 🦈 🐙 🐚 🦀 🦞 🦐 🦑 🐌 🦋 🐛 🐜 🐝 🐞 🦗 🕷 🕸 🦂 🦟 🦠"

class Memorygrid extends HTML.Form {
  upgradedCallback () {
    const { difficulty } = this.dataset
    this.difficulty = difficulty 
    this.total = difficulty ** 2
    this.animals = generateAnimalPairs(this.total / 2)
    this.showing = []
    this.hiding = []
    this.render()
  }
   
  render() {
    if (this.showing.length % 2 === 0) {
      const sum = this.showing.reduce((a, b) => a + b, 0)
      if (sum !== 0) {
        const uniques = this.showing.filter(v => this.showing.indexOf(v * -1) === -1)
        this.showing = this.showing.filter(v => !uniques.includes(v))
        this.hiding = this.hiding.concat(uniques)
      }
    }

    const remainingPairs = Math.ceil((this.animals.length - this.showing.length) / 2)
    const legend = remainingPairs ? `${remainingPairs} pair${remainingPairs > 1 ? 's': ''} remaining` : `Here's ya' 🌰!`
    console.log(this.hiding, this.showing)
    return render(this, html`
      <fieldset class="CardGrid m0 p0 br2 tc bb h-100" name="cards" @input=${event => this.showing.push(parseInt(event.target.id)) && this.render()}>
        <legend class="tc">
          <h1 class="tc p2 m0">${legend}</h1>
        </legend>
        <div class="dg gs fgs unselectable h-100 bg-grid" style=${`--grid-size: ${this.difficulty}`}>
          ${this.animals.map(([face, id]) => {
            const checked = this.showing.includes(id)
            const hiding = this.hiding.includes(id)
            const strId = id.toString()
            return html`
              <label
                class=${`t t-ease ${hiding ? 'bg-maroon mask-size-100 t-dur-short a-ease a-hide a-fill-backwards a-dur-long a-delay-long' : checked ? 'bg-aquamarine mask-size-100 t-dur-short ' : 'mask-size-0'} df justify-center items-center w-100 br2 mask-circle mask-norepeat mask-center`}
                style="--t-property: mask-size background-color"
                @animationEnd=${() => {this.hiding = this.hiding.filter(i => i === id); this.render()}}
                for=${strId}
              >
                <span class="df">${face}</span>
                <input
                  type="checkbox"
                  id=${strId}
                  class="absolute clip"
                  name=${face}
                  ?checked=${checked}
                  ?disabled=${checked}
                  required
                />
              </label>`
            })
          }
        </div>
      </fieldset>
    `)
  }
}

render(document.body, html`<${Memorygrid} data-difficulty="4" class="h-100" />`)

function random() {return Math.random() - .5}

function generateAnimalPairs(count) {
  const animalSet = ANIMALS
    .split(' ')
    .sort(random)
    .slice(0, count)
  // Returns set of face, value
  return [
    ...animalSet.map((s, i) => [s, i + 1]),
    ...animalSet.map((s, i) => [s, -i - 1])
  ].sort(random)
}
